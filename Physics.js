class Physics {

    constructor(entity) {
        this.entity = entity;
        this.hasCollision = false;
        this.collision = {}
    }

    AddCircleCollision(radius = 0, xOffset = 0, yOffset = 0) {
        this.hasCollision = true;
        this.collision = {
            radius,
            xOffset,
            yOffset
        }

        return this;
    }

    AddCollision(width, height, xOffset = 0, yOffset = 0) {
        this.hasCollision = true;
        this.collision = {
            width,
            height,
            xOffset,
            yOffset
        }

        return this;
    }

    DetectCollision() {
        const { x, y } = this.entity;
        const { radius, xOffset, yOffset } = this.collision;

        return this.entity.otherEntities.filter(entity => {
            const { x: entityX, y: entityY, physics } = entity;
            const { hasCollision, collision } = physics

            if (!hasCollision) return false;

            const distance = Math.sqrt(Math.pow((x + xOffset) - entityX, 2) + Math.pow((y + yOffset) - entityY, 2))

            return distance <= (radius + collision.radius);
        })
    }

    static GetRectangleBounds(x, y, xOffset, yOffset, width, height) {

        const topLeft = {
            x: x + xOffset,
            y: y + yOffset
        }
        const topRight = {
            x: (x + xOffset) + width,
            y: y + yOffset
        }
        const bottomLeft = {
            x: x + xOffset,
            y: (y + yOffset) + height
        }
        const bottomRight = {
            x: (x + xOffset) + width,
            y: (y + yOffset) + height
        }
        const center = {
            x: x + (topRight.x - topLeft.x) / 2,
            y: y + (bottomLeft.y - topLeft.y) / 2
        }

        return { 
            topLeft, 
            topRight, 
            bottomLeft, 
            bottomRight,
            center
        };
    }
}

export default Physics