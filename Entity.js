import { v4 } from 'uuid';
import Physics from './Physics';

class Entity {

    constructor(scene, config) {
        const { type, correctionFrequency, sorting } = config;

        if (!scene) throw new Error('no scene given in entity')
        if (!type) throw new Error('no type specified for entity')

        this.type = type;
        this.scene = scene;
        this.id = v4();
        this.correctionFrequency = correctionFrequency || 0;
        this.sorting = sorting || 0;
        this.physics = new Physics(this);

        this.x = 0;
        this.y = 0;
        this.isMine = false;
        this.otherEntities = [];

        this.width = 0;
        this.height = 0;

        this.invisable = false;
        this.isSprite = false;
        this.isText = false;
    }

    SetPosition(x, y) {
        this.x = x;
        this.y = y;

        this.ExecutePositionCorrection();
    }

    Start() {}

    Update() {}

    OnKeyDown(e) {}

    OnKeyUp(e) {}

    OnClick(e) {}

    OnKey(key) {
        return this.scene.pressing[key] === true;
    }

    Destroy() {
        this.scene.Destroy(this);
    }

    GetSprite() {}

    GetText() {}

    InitializePositionCorrection() {
        if (!this.isMine) return;

        if (this.correctionFrequency) {
            setInterval(() => {
                this.ExecutePositionCorrection()
            }, this.correctionFrequency * 1000);
        }
    }

    ExecutePositionCorrection() {
        this.scene.CorrectPosition(this.id, this.x, this.y)
    }
}

export default Entity;