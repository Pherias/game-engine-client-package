import Connection from './Connection';
import Physics from './Physics';

class Scene {
    constructor(name, config, registry) {
        this.name = name;
        this.wrapper = config.wrapper;
        this.canvas = config.canvas;
        this.frameRate = config.frameRate || 60;
        this.canvasContext = this.canvas.getContext('2d');
        this.id = config.sceneId;
        this.currentTime = Date.now();
        this.deltaTime = 0;
        
        this.connection = new Connection(this)
        this.connection.Connect(config.endpoint)
            .then((connection) => {
                const { entitiesToInstantiate, isMaster, connections } = connection;

                this.isMaster = isMaster;

                this.LoadScene()
                this.OnConnect({ connections });
                this.wrapper.OnConnected(this);

                this.InstantiateBufferedEntities(entitiesToInstantiate)
            })

        this.registry = registry;

        this.entities = [];
        this.pressing = [];
    }

    LoadScene() {
        console.log(`scene '${this.name}' loaded.`);
    }

    OnConnect() {};

    Update() {};

    OnKeyDown(e) {};

    OnKeyUp(e) {}

    OnClick(e) {}

    HandleEntityOnClick(e) {
        const { clientX: x, clientY: y } = e;

        this.entities.map(entity => {
            const distance = this.GetDistanceBetweenPositions((entity.x + (entity.width / 2)), (entity.y + (entity.height / 2)), x, y);

            if (distance <= (entity.width / 2))
                entity.OnClick();
        })
    }

    RunClock() {
        this.deltaTime = Date.now() - this.currentTime;

        if ((this.deltaTime) > (1000 / this.frameRate)) {
            this.currentTime = Date.now();

            this.UpdateFrame();
        }
    }

    UpdateFrame() {
        this.pressing = this.wrapper.pressing;

        this.Update();
        this.entities.forEach(entity => {
            entity.Update();
            
            if (entity.physics.hasCollision) entity.physics.DetectCollision(this);
        })
    }

    Instantiate(entity, xPosition = 0, yPosition = 0) {
        this.connection.Instantiate(entity, xPosition, yPosition);

        entity.x = xPosition;
        entity.y = yPosition;
        entity.isMine = true;

        this.AddEntity(entity);

        return entity;
    }

    RPC_Instantiate(entityDefinition) {
        const { type, id, xPosition, yPosition } = entityDefinition;

        const Entity_ = this.registry[type];
        if (!Entity_) throw new Error(`'${type}' not found in scene registry`)

        const entity = new Entity_(this);
        entity.id = id;

        entity.x = xPosition;
        entity.y = yPosition;
        entity.isMine = false;

        this.AddEntity(entity);

        return entity;
    }

    AddEntity(entity) {
        this.entities.push(entity);
        this.entities = this.entities.sort((a, b) => a.sorting > b.sorting ? 1 : -1)
        
        this.entities.forEach(entity => 
            entity.otherEntities = this.entities.filter(entity_ => entity_ !== entity))

        entity.InitializePositionCorrection();
        entity.Start();
    }

    InstantiateBufferedEntities(entitiesToInstantiate) {
        entitiesToInstantiate.forEach(entity => {
            const entityObject = this.RPC_Instantiate(entity)

            if (entity.bufferedActions)
                entity.bufferedActions.forEach(bufferedAction => entityObject[bufferedAction.action](bufferedAction.args))
        })
    }

    Destroy(entity) {
        this.connection.Destroy(entity.id);

        this.DestroyEntity(entity);
    }

    RPC_Destroy({ entityId }) {
        const entity = this.entities.find(entity => entity.id === entityId);

        this.DestroyEntity(entity);
    }

    DestroyEntity(entity) {
        const entityIndex = this.entities.indexOf(entity);

        this.entities.splice(entityIndex, 1);
    }

    RPC(entity, action, bufferMode, args) {
        this.connection.RPC(entity, action, bufferMode, args)
    }

    RPC_Callback(entityId, action, args) {
        const entity = this.entities.find(entity => entity.id === entityId)

        if (!entity) return;

        entity[action](args);
    }

    GetEntitiesOfType(type) {
        if (!this.entities) return [];

        return this.entities.filter(entity => entity.type === type);
    }

    GetEntityById(id) {
        return this.entities.find(entity => entity.id === id);
    }

    CorrectPosition(entityId, x, y) {
        this.connection.CorrectPosition(entityId, x, y);
    }

    RPC_CorrectPosition(entityId, x, y) {
        const entity = this.GetEntityById(entityId);

        if (!entity) return;

        entity.x = x;
        entity.y = y;
    }

    OnConnectionJoined(connection) {}

    GetDistanceBetweenPositions(x1, y1, x2, y2) {
        return Math.sqrt( Math.pow((x1-x2), 2) + Math.pow((y1-y2), 2) );
    }
}

export default Scene;