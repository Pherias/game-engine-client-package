class Animation {

    constructor(entity) {
        this.entity = entity;
        this.animations = [];

        this.trigger = null;

        this.currentIndex = 0;
        this.currentAnimation = {}

        this.InitializeInterval(1000)
    }

    InitializeInterval(frequence) {
        if (this.interval) clearInterval(this.interval);

        this.interval = setInterval(() => {
            if (!this.HasAnimationToPlay()) return;

            if ((this.currentIndex + 1) === this.currentAnimation.list.length)
                this.currentIndex = 0;
            else this.currentIndex += 1;
        }, frequence);
    }

    AddAnimation(triggerName, animationList, refreshFrequence = 100) {
        if (this.animations.find(animation => animation.name === triggerName))
            throw new Error(`animation '${triggerName}' already configured`);
        
        this.animations.push({
            name: triggerName,
            list: animationList,
            refreshFrequence
        })

        return this;
    }

    Trigger(triggerName) {
        this.currentIndex = 0;
        const animation = this.animations.find(animation => animation.name === triggerName);

        if (!animation) throw new Error(`animation with name ${triggerName} was not found`);

        this.currentAnimation = animation;

        this.InitializeInterval(animation.refreshFrequence)
    }

    GetCurrentFrame() {
        if (!this.HasAnimationToPlay()) return null;

        return this.currentAnimation.list[this.currentIndex];
    }

    HasAnimationToPlay() {
        return this.currentAnimation && this.currentAnimation.list.length > 0;
    }
}

export default Animation;