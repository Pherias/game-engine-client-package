import { io } from 'socket.io/client-dist/socket.io'

class Connection {
    /**
     *
     * @param {Configuration} config
     * Attributes:
     * - endpoint eg. 'localhost:3030'
     */
    constructor(scene) {
        this.scene = scene;

        this.connections = [];
    }

    async Connect(endpoint) {
        return new Promise((resolve, reject) => {
            this.socket = io(endpoint, { query: { sceneId: this.scene.id } })

            this.socket.on('connected', (e) => {
                const { entitiesToInstantiate, isMaster, connections } = e;
                resolve({ connection: new Connection(this.socket), entitiesToInstantiate, isMaster, connections });
            });

            this.socket.on('user_connected', (e) => {
                const { connections, newConnection } = e;
                this.connections = connections;
                console.log('new connection entered scene: ' + newConnection);

                this.scene.OnConnectionJoined(newConnection);
            })
            this.socket.on(`instantiate_${this.scene.id}`, (args) => {
                this.scene.RPC_Instantiate(args)
            })
            this.socket.on(`destroy_${this.scene.id}`, (args) => {
                this.scene.RPC_Destroy(args);
            })
            this.socket.on(`RPC_${this.scene.id}`, ({ entityId, action, args }) => {
                this.scene.RPC_Callback(entityId, action, args);
            })
            this.socket.on(`correct_position_${this.scene.id}`, ({ entityId, x, y }) => {
                this.scene.RPC_CorrectPosition(entityId, x, y);
            })
        })
    }

    Disconnect() {
        this.socket.disconnect();
    }

    Instantiate(entity, xPosition, yPosition) {
        const type = entity.type;
        const { id } = entity;
        const sceneId = this.scene.id;

        this.socket.emit(`instantiate_${sceneId}`, {
            type,
            id,
            xPosition,
            yPosition
        })
    }

    Destroy(entityId) {
        const sceneId = this.scene.id;

        this.socket.emit(`destroy_${sceneId}`, { entityId })
    }

    RPC(entity, action, bufferMode, args) {
        const sceneId = this.scene.id;

        this.socket.emit(`RPC_${sceneId}`, {
            entityId: entity.id,
            action,
            bufferMode,
            args
        })
    }

    CorrectPosition(entityId, x, y) {
        const sceneId = this.scene.id;

        this.socket.emit(`correct_position_${sceneId}`, {
            entityId,
            x,
            y
        })
    }
}

export default Connection;